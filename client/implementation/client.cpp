#include "../include/client.hpp"
// #include <openssl/sha.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <functional>
#include <boost/property_tree/json_parser.hpp>

namespace wallet
{
namespace client
{
namespace
{
    static bool CURL_INITED = false;

    static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
    {
        (reinterpret_cast<std::string*>(userp))->append(reinterpret_cast<char*>(contents), size * nmemb);
        return size * nmemb;
    }

    std::string CurlPost(CURL* curl_handle, const std::string& server, const std::string& msg)
    {
        std::string response_buffer;
        curl_easy_setopt(curl_handle, CURLOPT_URL, server.c_str());
        curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDS, msg.c_str());
        curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, &response_buffer);

        curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, true);
        auto res = curl_easy_perform(curl_handle);
        if(res != CURLE_OK)
        {
            std::stringstream ss;
            ss << "Could not POST on server. Error: " << res << " (" << curl_easy_strerror(res) << ")" << std::endl;
            ss << "Message was: " << msg << std::endl;
            std::string error = ss.str();
            throw WalletException(error.c_str());
        }

        return response_buffer;
    }

    // TODO: maybe b32-address as string is better
    std::string GenAddress()
    {
        std::string random_str = common::GenerateRandomString(10);
        return std::to_string(std::hash<std::string>{}(random_str));
    }

//    client::Client::AddressMap ReadFromFile(const std::string& file)
//    {
//         std::ifstream file_stream(file);
//         std::string line;
//         client::Client::AddressMap result;
//         while(std::getline(file_stream, line))
//         {
//             if(line.empty())
//                 continue;
//             auto whitespace_pos = line.find(' ');
//             std::string address = line.substr(0, whitespace_pos);
//             common::wallet_amount_type amount = common::AmountFromString(line.substr(whitespace_pos, std::string::npos));
//             result[address] = amount;
//         }
//         if(file_stream.is_open())
//            file_stream.close();
//         return result;
//    }

//    void WriteWalletInfoToFile(const std::string& file, const client::Client::AddressMap& wallet_info)
//    {
//       std::ofstream file_stream(file);
//       for(auto el : wallet_info)
//       {
//          file_stream << el.first << " " << el.second << std::endl;
//       }
//       if(file_stream.is_open())
//          file_stream.close();
//    }

    std::string SerializeMapToJson(const std::map<std::string, std::string>& map)
    {
        boost::property_tree::ptree pt;
        std::stringstream ss;
        for(auto& el : map)
        {
            pt.put(el.first, el.second);
        }

        boost::property_tree::write_json(ss, pt);
        return ss.str();
    }

    std::string ParseSendArg(std::vector<std::string>& params)
    {
        std::map<std::string, std::string> json;
        json["client_address"] = params[0];
        json["recipient_address"] = params[1];
        json["amount"] = params[2];
        json["client_token"] = params[3];

        return SerializeMapToJson(json);
    }
} // anonymous namespace

Client::~Client()=default;

void Client::Init(ArgParser::Args args)
{
    auto it = args.find("--server");
    if(it == args.end())
        throw WalletException{"Could not find --server argument"};

    m_server_address = (*it).second;
    if(!CURL_INITED)
    {
        if(curl_global_init(CURL_GLOBAL_ALL) != CURLE_OK)
            throw WalletException{"Could not init CURL"};
        CURL_INITED = true;
    }

    curl_handle = curl_easy_init();
    if(!curl_handle)
        throw WalletException{"Could not init CURL"};

    SyncWalletInfo();
}

void Client::Run()
{
    for(;;)
    {
        std::cout << "> ";
        std::string input;
        std::cin >> input;
        if(input == "s" || input == "send")
        {
            std::vector<std::string> params;
            params.reserve(3);
            std::string sender;
            std::string recipient;
            std::string amount;
            std::cin >> sender >> recipient >> amount;
            common::wallet_amount_type amount_from_str = common::AmountFromString(amount);
            auto it = m_wallet_info.find(sender);
            if(it == m_wallet_info.end())
            {
               std::cout << "Address " << sender << " was not found." << std::endl;
               continue;
            }

            if(amount_from_str > m_wallet_info[sender])
            {
               std::cout << "Not enough money for address " << sender << std::endl;
               continue;
            }
            params.push_back(sender);
            params.push_back(recipient);
            params.push_back(amount);
            params.push_back(m_token);
            try
            {
                auto json = ParseSendArg(params);
                auto response = SendCommand(ClientCommand::SEND, json);
                SyncWalletInfo();
            }
            catch(const WalletException& exc)
            {
                std::cout << exc.what() << std::endl;
            }
        }
        else if(input == "g" || input == "generate")
        {
            try
            {
                std::string new_address = GenAddress();
                m_wallet_info[new_address] = 0.0;
                std::map<std::string, std::string> json_map;
                json_map["client_token"] = GetToken();
                json_map["client_address"] = new_address;
                SendCommand(ClientCommand::CREATE, SerializeMapToJson(json_map));
                std::cout << "Registered new address: " << new_address << std::endl;
                SyncWalletInfo();
            }
            catch(const WalletException& exc)
            {
                std::cout << exc.what() << std::endl;
            }
        }
        else if(input == "d" || input == "delete")
        {
            // TODO: delete an address
        }
        else if(input == "ls" || input == "list")
        {
            for(auto& el : m_wallet_info)
            {
                std::cout << "Address: " << el.first << " Amount: " << el.second << std::endl;
            }
        }
        else if(input == "h" || input == "help")
        {
            std::cout << "Welcome to wallet prototype client!" << std::endl;
            std::cout << "Commands:" << std::endl;
            std::cout << "'s' or 'send <sender address> <recipient address> <amount>' - send amount of money from one address to another" << std::endl;
            std::cout << "'g' or 'generate' - generate new address" << std::endl;
            std::cout << "'ls' or 'list' - list all addresses" << std::endl;
            std::cout << "'d' or 'delete' - delete address (this will not remove it on server)" << std::endl;
            std::cout << "'q' or 'quit' - quit client" << std::endl;
            std::cout << "'h' or 'help' - show this message" << std::endl;
        }
        else if(input == "q" || input == "quit")
        {
            std::cout << "Quitting..." << std::endl;
            break;
        }
        else
        {
            std::cout << "Unrecognized command. Type 'help' for list of the commands." << std::endl;
        }
    }
}

std::string Client::SendCommand(ClientCommand cmd, const std::string& arg)
{
    std::string response;
    switch(cmd)
    {
    case ClientCommand::SEND:
        response = CurlPost(curl_handle, m_server_address + "/send", arg);
        break;
    case ClientCommand::CREATE:
        response = CurlPost(curl_handle, m_server_address + "/register", arg);
        break;
    case ClientCommand::UPDATE:
        response = CurlPost(curl_handle, m_server_address + "/get_info", arg);
        break;
    default:
        break;
    }

    return response;
}

std::string Client::GetToken()
{
    static const char* token_file_name = "token";
    std::ifstream token_file(token_file_name, std::ifstream::in);
    std::string token;
    if(token_file.is_open())
    {
        token_file >> token;
        token_file.close();
        if(!token.empty())
        {
            m_token = token;
            return m_token;
        }
    }

    if(token.empty())
        m_token = GenAddress();
    return m_token;
}

void Client::SaveToken()
{
    static const char* token_file_name = "token";
    std::ofstream token_file(token_file_name, std::ofstream::out | std::ofstream::trunc);
    if(token_file.is_open())
    {
        token_file << m_token;
        token_file.close();
    }
}

void Client::SyncWalletInfo()
{
    boost::property_tree::ptree root;
    std::stringstream ss;
    root.put("client_token", GetToken());
    boost::property_tree::write_json(ss, root);
    std::string response = SendCommand(ClientCommand::UPDATE, ss.str());

    boost::property_tree::ptree response_tree;
    std::stringstream reply_stream;
    reply_stream << response;
    std::cout << "Got addresses from server: " << response;
    boost::property_tree::read_json(reply_stream, response_tree);
    for(boost::property_tree::ptree::value_type& element : response_tree.get_child("addresses"))
    {
        m_wallet_info[element.first] = common::AmountFromString(element.second.data());
    }
}

void Client::Shutdown()
{
    curl_easy_cleanup(curl_handle);
    curl_global_cleanup();
    SaveToken();
}

const char *WalletException::what() const noexcept
{
    return cause.c_str();
}

} // namespace client
} // namespace wallet
