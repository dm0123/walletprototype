#include <iostream>
#include <exception>
#include <include/argparser.hpp>

#include "../include/client.hpp"

int main(int argc, char** argv)
{
    try
    {
        wallet::ArgParser args(argc, argv);
        wallet::ArgParser::ArgsList tmpl{ "--server", "--send", "--generate", "--list" };
        auto parsed_args = args.Parse(tmpl);
        wallet::client::Client client;
        client.Init(parsed_args);
        client.Run();
        client.Shutdown();
    }
    catch(const wallet::client::WalletException& wallet_exc)
    {
        std::cout  << wallet_exc.what() << std::endl;
        return 1;
    }
    catch(const std::exception& exc)
    {
        std::cout << "Caught an exception:" << std::endl << exc.what() << std::endl;
        return 1;
    }
    catch(...)
    {
        std::cout << "Caught unknown exception:" << std::endl;
        return 1;
    }
    return 0;
}
