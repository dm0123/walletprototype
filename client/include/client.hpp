#pragma once
#include <string>
#include <exception>
#include <map>
#include <vector>
#include <include/common.hpp>
#include <curl/curl.h>
#include <include/argparser.hpp>

namespace wallet
{
namespace client
{
class WalletException : public std::exception
{
public:
    WalletException(const char* cause) : cause(cause)
    {
    }

    ~WalletException() override=default;

    const char* what() const noexcept override;
private:
    std::string cause;
};

enum class ClientCommand : int
{
    SEND,
    CREATE,
    REMOVE,
    UPDATE,
    LIST
};

class Client
{
public:
   using AddressMap = std::map<std::string, common::wallet_amount_type>;
    Client()=default;
    ~Client();

    void Init(ArgParser::Args args);
    void Run();
    void Shutdown();
    std::string SendCommand(ClientCommand cmd, const std::string& arg);
    std::string GenerateAddress();
private:
    CURL* curl_handle;
    std::string m_server_address;
    AddressMap m_wallet_info;
    std::string m_token;

    std::string GetToken();
    void SaveToken();
    void SyncWalletInfo();
};
} // namespace client
} // namespace wallet
