#include "../include/business_logic.hpp"
#include "../include/exceptions.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <functional>
#include <algorithm>

namespace wallet
{
namespace server
{
namespace bl
{
namespace
{
   BusinessLogic::ClientsMap::const_iterator FindAddress(const BusinessLogic::ClientsMap& clients, const std::string& address)
    {
        auto it = std::find_if(clients.begin(), clients.end(),
                              [&address](const std::pair<std::string, std::vector<std::pair<std::string, common::wallet_amount_type>>>& el)
        {
            auto element_it = std::find_if(el.second.begin(), el.second.end(), [&address](const std::pair<std::string, common::wallet_amount_type>& addr_el)
            {
                if(addr_el.first == address)
                    return true;
                return false;
            });
            if(element_it == el.second.end())
               return false;
           return true;
        });

        return it;
    }
} // anonymous namespace

void BusinessLogic::Init(db::SQLiteDb& database)
{
    std::vector<db::Row> rows;
    auto success = database.GetRows(rows);
    if(!success)
       throw BlException("Error reading database");
    for(auto row : rows)
    {
       m_clients[row.client_token].push_back(std::make_pair(row.client_address, row.amount));
    }
}

void BusinessLogic::Sync(db::SQLiteDb& database)
{
    for(auto element : m_clients)
    {
        for(auto address_it : element.second)
        {
            db::Row row;
            if(database.TryGetRowByAddress(address_it.first, row))
            {
                row.amount = address_it.second;
                database.UpdateRow(row);
            }
            else
                database.InsertRow(db::Row{ element.first, address_it.first, address_it.second });
        }
    }
}

void BusinessLogic::Register(const std::string& address, const std::string& client_token)
{
    auto it = FindAddress(m_clients, address);
    if(it != m_clients.end())
    {
        std::stringstream ss;
        ss << "Register: client " << address << ":" << client_token << " already registered" << std::endl;
        std::string err = ss.str();
        throw ServerException{err.c_str()};
    }

    m_clients[client_token].push_back(std::make_pair(address, 0));
}

void BusinessLogic::Send(const std::string& from_address, const std::string& to_address, common::wallet_amount_type amount)
{
    auto from_it = FindAddress(m_clients, from_address);
    if(from_it == m_clients.end())
    {
        std::cout << "Send: sender " << from_address << " was not found." << std::endl;
        return;
    }

    auto to_it = FindAddress(m_clients, to_address);
    if(to_it == m_clients.end())
    {
        std::cout << "Send: recipient " << to_address << " was not found." << std::endl;
        return;
    }

    auto& address_list_from = m_clients[from_it->first];
    auto from_address_it = std::find_if(address_list_from.begin(), address_list_from.end(), [&from_address](const std::pair<std::string, common::wallet_amount_type>& el)
    {
        if(el.first == from_address)
           return true;
        return false;
    });

    common::wallet_amount_type tmp_amount = from_address_it->second;
    if(tmp_amount < amount)
    {
        std::stringstream ss;
        ss << "Not enough money on " << from_address << std::endl;
        std::string err = ss.str();
        throw BlException{ err.c_str() };
    }
    tmp_amount-= amount;
    from_address_it->second = tmp_amount;

    auto& address_list_to = m_clients[to_it->first];
    auto address_list_to_it = std::find_if(address_list_to.begin(), address_list_to.end(), [&to_address](const std::pair<std::string, common::wallet_amount_type>& el)
    {
        if(el.first == to_address)
           return true;
        return false;
    });
    address_list_to_it->second += amount;
}
} // namespace bl
} // namespace server
} // namespace wallet
