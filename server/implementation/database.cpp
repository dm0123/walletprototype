#include "../include/database.hpp"
#include <sstream>
#include <string>
#include <iostream>

namespace wallet
{
namespace server
{
namespace db
{
namespace
{
const static char* create_table = "CREATE TABLE ADDRESSES("  \
   "ID INTEGER PRIMARY KEY AUTOINCREMENT," \
   "CLIENT_TOKEN   TEXT    NOT NULL," \
   "ADDRESS        TEXT    NOT NULL," \
   "AMOUNT         DOUBLE     NOT NULL);";

static int sqlite_select_callback(void* data, int argc, char** argv, char** azColName)
{
    std::cout << "sqlite_select_callback called" << std::endl;
    if(data)
    {
        if(argc != 4)
            return 0;
        auto& rows = *(reinterpret_cast<std::vector<Row>*>(data));
        std::string amount {argv[3]};
        rows.push_back(Row{argv[1], argv[2], stod(amount)});
    }
    return 0;
}

static int sqlite_callback(void* /*data*/, int /*argc*/, char** /*argv*/, char** /*azColName*/)
{
    return 0;
}
} // anonymous namespace


void SQLiteDb::Init(const std::string& db_filename)
{
    m_db_filename = db_filename;
    try
    {
        std::cout << "Db: Trying to get existing ADDRESSES..." << std::endl;
        std::vector<Row> rows;
        GetRows(rows);
        std::cout << "Db: Success!" << std::endl;
    }
    catch(const DbException&)
    {
        std::cout << "Db: Cannot open existing ADDRESSES. Creating new one..." << std::endl;
        // Assuming there is no table
        sqlite3* database = OpenDb();
        char* err_msg = nullptr;
        auto rc = sqlite3_exec(database, create_table, sqlite_callback, nullptr, &err_msg);
        if( rc != SQLITE_OK )
        {
            CloseDb(database);
            throw DbException(err_msg);
        }

        CloseDb(database);
        std::cout << "Db: Success!" << std::endl;
    }
}

bool SQLiteDb::InsertRow(const server::db::Row& row)
{
    sqlite3* database = OpenDb();
    std::stringstream query_str;
    query_str << "INSERT INTO ADDRESSES  " \
             "VALUES(NULL, " <<  "'" << row.client_token << "', '" << row.client_address << "', " << row.amount << ");";
    std::string query = query_str.str();
    char* err_msg = nullptr;
    auto rc = sqlite3_exec(database, query.c_str(), sqlite_callback, nullptr, &err_msg);
    if( rc != SQLITE_OK )
    {
        CloseDb(database);
        throw DbException(err_msg);
    }

    CloseDb(database);
    return true;
}

bool SQLiteDb::RemoveRow(const server::db::Row& /*row*/)
{
    // TODO:
    return true;
}

bool SQLiteDb::UpdateRow(const server::db::Row& row)
{
    sqlite3* database = OpenDb();
    std::stringstream query_str;
    query_str << "UPDATE ADDRESSES  " \
             "SET AMOUNT=" << row.amount << " WHERE ADDRESS='" << row.client_address << "';";
    std::string query = query_str.str();
    char* err_msg = nullptr;
    auto rc = sqlite3_exec(database, query.c_str(), sqlite_callback, nullptr, &err_msg);
    if( rc != SQLITE_OK )
    {
        CloseDb(database);
        throw DbException(err_msg);
    }

    CloseDb(database);
    return true;
}

bool SQLiteDb::TryGetRowByAddress(std::string address, Row& row_to_get)
{
    std::stringstream query_stream;
    query_stream << "SELECT * FROM ADDRESSES WHERE ADDRESS='" << address << "';";
    std::string query = query_stream.str();
    sqlite3* database = OpenDb();
    char* err_msg = nullptr;
    std::vector<Row> rows;
    auto rc = sqlite3_exec(database, query.c_str(), sqlite_select_callback, reinterpret_cast<void*>(&rows), &err_msg);
    if( rc != SQLITE_OK )
    {
        CloseDb(database);
        throw DbException(err_msg);
    }

    CloseDb(database);
    if(rows.size() > 0)
    {
        row_to_get = rows[0];
        return true;
    }

    return false;
}

bool SQLiteDb::GetRows(std::vector<Row>& rows)
{
    static const char* query = "SELECT * FROM ADDRESSES;";
    sqlite3* database = OpenDb();
    char* err_msg = nullptr;
    auto rc = sqlite3_exec(database, query, sqlite_select_callback, reinterpret_cast<void*>(&rows), &err_msg);
    if( rc != SQLITE_OK )
    {
        CloseDb(database);
        throw DbException(err_msg);
    }

    CloseDb(database);
    return true;
}

sqlite3* SQLiteDb::OpenDb()
{
    sqlite3* database;
    auto rc = sqlite3_open(m_db_filename.c_str(), &database);
    if(rc != SQLITE_OK)
    {
        throw DbException("Cannot open SQLite3 database.");
    }

    return database;
}

void SQLiteDb::CloseDb(sqlite3* database)
{
    sqlite3_close(database);
}

const char *DbException::what() const noexcept
{
    return cause.c_str();
}

} // namespace db
} // namespace server
} // namespace wallet
