#include <iostream>
#include <exception>

#include "../include/exceptions.hpp"
#include "../include/server.hpp"

int main(int argc, char** argv)
{
    try
    {
        wallet::ArgParser args(argc, argv);
        wallet::ArgParser::ArgsList tmpl{ "--port", "--cert", "--key", "--help" };
        auto parsed_args = args.Parse(tmpl);
        auto it = parsed_args.find("--help");
        if(it != parsed_args.end())
        {
            std::cout << "Wallet server prototype help." << std::endl;
            std::cout << "'--help' - display this message and exit." << std::endl;
            std::cout << "'--port' - port which server will use." << std::endl; 
            std::cout << "'--cert' - server certificate file path for https (default is 'cert.pem')." << std::endl;
            std::cout << "'--key' -  key file path for certificate (default is 'key.pem')." << std::endl;
            return 0;
        }


        std::string cert{"cert.pem"};
        std::string key{"key.pem"};

        it = parsed_args.find("--cert");
        if(it != parsed_args.end())
           cert = (*it).second;

        it = parsed_args.find("--key");
        if(it != parsed_args.end())
            key = (*it).second;
        wallet::server::Server server(cert, key, parsed_args);
        server.Init();
        server.Run();
        server.Shutdown();
    }
    catch(const wallet::server::db::DbException& db_exc)
    {
        std::cout << "Database exception occured: " <<  db_exc.what() << std::endl;
    }
    catch(const std::exception& e)
    {
        std::cout << "Exception during running server:\n" << e.what() << std::endl;
        return 1;
    }
    catch(...)
    {
        std::cout << "Uknown exception during running server.\n" << std::endl;
        return 1;
    }
    return 0;
}
