#include "../include/server.hpp"
#include <boost/property_tree/json_parser.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <memory>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

namespace wallet
{
namespace server
{
namespace
{
void Error(std::shared_ptr<UnderlyingServer::Request> request, const SimpleWeb::error_code & ec)
{
    std::stringstream ss;
    ss << "Server exception: " << std::endl << "Code: " << ec << std::endl << "Request body" << request->content.string();
    std::string error = ss.str();
    std::cout << error;
}
} // anonymous namespace

Server::Server(const std::string& server_cert_path, const std::string& server_key_path, ArgParser::Args args)
             : m_server(server_cert_path, server_key_path), m_args(args)
{
}

void Server::Init()
{
    auto it = m_args.find("--port");
    if(it == m_args.end())
        m_server.config.port = 80;
    else
        m_server.config.port = static_cast<unsigned short>(std::stoi((*it).second));
    std::cout << "Running on port " << m_server.config.port << std::endl;
    m_database.Init("server.sqlite");
    m_logic.Init(m_database);
    RegisterMethods();
}

void Server::DefaultGet(std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> /*request*/)
{
    std::stringstream ss;
    ss << "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">" \
    "<html><body><p>Hello from wallet server!</p>";
    ss << "<div>";
    for(const auto& el : m_logic.GetClients())
    {
        for(const auto& address : el.second)
            ss << "<div>Address: " << address.first << ", Amount: " << address.second << "</div>";
    }
    ss << "</div></body></html>";
    std::string default_page = ss.str();
    response->write(SimpleWeb::StatusCode::success_ok, default_page);
}

void Server::SendMoney(std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request)
{
    std::cout << "Send money called" << std::endl;
    std::string content = request->content.string();
    std::cout << "Recieved content: " << content << std::endl;
    try
    {
       boost::property_tree::ptree property_tree;
       std::stringstream buf;
       buf << content;
       boost::property_tree::read_json(buf, property_tree);

       std::string client_token = property_tree.get<std::string>("client_token");
       std::string client_address = property_tree.get<std::string>("client_address" ,"");
       std::string recipient_address = property_tree.get<std::string>("recipient_address", "");
       std::string amount_to_send= property_tree.get<std::string>("amount", "");
       m_logic.Send(client_address, recipient_address, std::stod(amount_to_send));
       m_logic.Sync(m_database);
    }
    catch(const boost::exception& exc)
    {
       std::string exc_info = boost::diagnostic_information(exc);
       response->write(SimpleWeb::StatusCode::server_error_internal_server_error, exc_info.c_str());
       return;
    }
    catch(const std::exception& std_exc)
    {
        response->write(SimpleWeb::StatusCode::server_error_internal_server_error, std_exc.what());
        return;
    }
    *response << "HTTP/1.1 200 OK\r\nContent-Length: " << content.length() << "\r\n\r\n"
              << content;
}

void Server::RegisterClient(std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request)
{
    std::cout << "Register client called" << std::endl;
    std::string content = request->content.string();
    std::cout << "Recieved content: " << content << std::endl;
    try
    {
        boost::property_tree::ptree property_tree;
        std::stringstream buf;
        buf << content;
        boost::property_tree::read_json(buf, property_tree);

        std::string client_address = property_tree.get<std::string>("client_address" ,"");
        std::string client_token = property_tree.get<std::string>("client_token", "");
        m_logic.Register(client_address, client_token);
        m_logic.Sync(m_database);
    }
    catch(const boost::exception& exc)
    {
        std::string exc_info = boost::diagnostic_information(exc);
        std::cout << "Failed to register client ";
        response->write(SimpleWeb::StatusCode::server_error_internal_server_error, exc_info.c_str());
        return;
    }
    catch(const std::exception& std_exc)
    {
        std::cout << "Failed to register client: " << std_exc.what() << std::endl;
        response->write(SimpleWeb::StatusCode::server_error_internal_server_error, std_exc.what());
        return;
    }
    *response << "HTTP/1.1 200 OK\r\nContent-Length: " << content.length() << "\r\n\r\n"
              << content;
}

void Server::GetInfo(std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request)
{
    std::cout << "GetInfo called" << std::endl;
    std::string content = request->content.string();
    std::cout << "Recieved content: " << content << std::endl;
    try
    {
        boost::property_tree::ptree property_tree;
        std::stringstream buf;
        buf << content;
        boost::property_tree::read_json(buf, property_tree);
        std::string client_token = property_tree.get<std::string>("client_token", "");

        auto clients = m_logic.GetClients();
        boost::property_tree::ptree reply_root;
        boost::property_tree::ptree send_pt;
        std::stringstream send_ss;

        for(auto& el : clients[client_token])
        {
            send_pt.put(el.first, el.second);
        }

        reply_root.add_child("addresses", send_pt);
        boost::property_tree::write_json(send_ss, reply_root);
        response->write(SimpleWeb::StatusCode::success_ok, send_ss.str());
    }
    catch(const std::exception& exc)
    {
       response->write(SimpleWeb::StatusCode::server_error_internal_server_error, exc.what());
    }
}

void Server::RegisterMethods()
{
    std::cout << "Register methods" << std::endl;
    m_server.resource["^/send"]["POST"] = [this](std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request)
    {
       SendMoney(response, request);
    };

    m_server.resource["^/register"]["POST"] = [this](std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request)
    {
       RegisterClient(response, request);
    };

    m_server.resource["^/get_info"]["POST"] = [this](std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request)
    {
       GetInfo(response, request);
    };

    m_server.default_resource["GET"] = [this](std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request)
    {
        DefaultGet(response, request);
    };

    m_server.on_error = &Error;
}

void Server::Run()
{
   m_server.start(); // TODO: make multithreaded
}

void Server::Shutdown()
{
   m_logic.Sync(m_database);
}

const char *ServerException::what() const noexcept
{
    return cause.c_str();
}

bl::BlException::~BlException()=default;

} // namespace server
} // namespace wallet
