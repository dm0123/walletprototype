#pragma once

#include <map>
#include <vector>
#include <include/common.hpp>
#include "../include/database.hpp"
#include "../include/exceptions.hpp"

namespace wallet
{
namespace server
{
namespace bl
{
/**
 * @brief The BusinessLogic class
 * @details Controller for server
 */
class BusinessLogic
{
public:
    // TODO: map<client_token, pair<address, amount>>
    using ClientsMap = std::map<std::string, std::vector<std::pair<std::string, common::wallet_amount_type>>>;
    BusinessLogic()=default;
    ~BusinessLogic()=default;

    /// Init BusinessLogic
    void Init(db::SQLiteDb& database);

    /// Call Sync when BusinessLogic needs to store data in database
    void Sync(db::SQLiteDb& database);

    /// Register client
    void Register(const std::string& address, const std::string& client_token);

    /// Send money from one client to another
    void Send(const std::string& from_address, const std::string& to_address, common::wallet_amount_type amount);

    //Get clients
    const ClientsMap& GetClients() { return m_clients; }

private:
    ClientsMap m_clients;
};
} // namespace bl
} // namespace server
} // namespace wallet
