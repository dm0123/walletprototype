#pragma once

#include <sqlite3.h>
#include <string>
#include <exception>
#include <vector>

#include <include/common.hpp>

namespace wallet
{
namespace server
{
namespace db
{
class DbException : public std::exception
{
public:
    DbException(const char* cause) : cause(cause)
    {
    }

    ~DbException() override=default;

    const char* what() const noexcept override;

private:
    std::string cause;
};

// TODO: authorization
struct Row
{
    std::string client_token;
    std::string client_address;
    common::wallet_amount_type amount;
};

/**
 * @brief The SQLiteDb class
 * @details Database representation for server
 */
class SQLiteDb
{
public:
    SQLiteDb()=default;
    ~SQLiteDb()=default;

    /**
     * @brief Init
     * @param db_filename database filename
     */
    void Init(const std::string& db_filename);

    /**
     * @brief InsertRow inserts a Row into database
     * @param row row to insert
     * @return success
     */
    bool InsertRow(const Row& row);

    /**
     * @brief RemoveRow Deletes a Row from database
     * @param row row to remove
     * @return success
     */
    bool RemoveRow(const Row& row);

    /**
     * @brief UpdateRow Updates a Row in database
     * @param row row to update
     * @return success
     */
    bool UpdateRow(const Row& row);

    /**
     * @brief TryGetRowByAddress Get a row by its address
     * @param address address for row to get
     * @param row_to_get Row object which be used to store db information
     * @return success
     */
    bool TryGetRowByAddress(std::string address, Row& row_to_get);

    /**
     * @brief GetRows Select all rows from database
     * @param rows Vector of rows to return
     * @return success
     */
    bool GetRows(std::vector<Row>& rows);
private:
    std::string m_db_filename;

    sqlite3* OpenDb();
    void CloseDb(sqlite3* database);
};
}
} // namespace server
} // namespace wallet
