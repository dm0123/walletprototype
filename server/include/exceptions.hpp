#pragma once

#include <exception>
#include <string>

namespace wallet
{
namespace server
{
class ServerException : public std::exception
{
public:
    ServerException(const char* cause) : cause(cause)
    {
    }

    ~ServerException() override=default;

    const char* what() const noexcept override;

private:
    std::string cause;
};

namespace bl
{
class BlException : public ServerException
{
public:
   BlException(const char* cause) : ServerException(cause)
   {
   }

   ~BlException() override;
};
} // namespace bl

namespace db
{
   class DbException;
} // namespace db
} // namespace server
} // namespace wallet
