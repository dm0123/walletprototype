#pragma once

#include <server_https.hpp>
#include <crypto.hpp>
#include <string>

#include <include/argparser.hpp>
#include "../include/database.hpp"
#include "../include/exceptions.hpp"
#include "../include/business_logic.hpp"

namespace wallet
{
namespace server
{
using UnderlyingServer = SimpleWeb::Server<SimpleWeb::HTTPS>;

class Server
{
public:
    Server(const std::string& server_cert_path,
           const std::string& server_key_path,
           ArgParser::Args args);

    ~Server()=default;
    void Init();
    void Run();
    void Shutdown();
private:
    using HttpsServer = SimpleWeb::Server<SimpleWeb::HTTPS>;

    void DefaultGet(std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request);
    void SendMoney(std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request);
    void RegisterClient(std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request);
    void GetInfo(std::shared_ptr<UnderlyingServer::Response> response, std::shared_ptr<UnderlyingServer::Request> request);

    void RegisterMethods();

    HttpsServer m_server;
    ArgParser::Args m_args;
    db::SQLiteDb m_database;
    bl::BusinessLogic m_logic;
};
} // namespace server
} // namespace wallet
