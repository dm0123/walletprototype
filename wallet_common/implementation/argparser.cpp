#include "../include/argparser.hpp"
#include <algorithm>

namespace wallet
{
ArgParser::ArgParser(int argc, char** argv)
{
   for(int i = 0; i < argc; i++)
   {
      std::string arg(argv[i]);
      m_args.push_back(arg);
   }
}

ArgParser::Args ArgParser::Parse(ArgsList tmpl)
{
   ArgParser::Args result;
   for(auto arg : tmpl)
   {
      auto it = std::find(m_args.begin(), m_args.end(), arg);
      if(it != m_args.end() && it + 1 != m_args.end())
      {
         if((*it)[0] == '-' && (*it)[1] == '-')
         {
            auto next = it + 1;
            result[*it] = *next;
         }
      }
   }

   return result;
};
} // namespace wallet
