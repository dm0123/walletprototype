#pragma once

#include <vector>
#include <string>
#include <exception>
#include <map>

namespace wallet
{
class ParseException : public std::exception
{
};

class ArgParser
{
public:
   using ArgsList = std::vector<std::string>;
   using Args = std::map<std::string, std::string>;

   ArgParser(int argc, char** argv);
   ~ArgParser()=default;

   Args Parse(ArgsList tmpl);
private:
   ArgsList m_args;
};
} // namespace wallet
