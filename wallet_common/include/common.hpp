#pragma once
#include <vector>
#include <sstream>
#include <string>
#include <ctime>

namespace wallet
{
namespace common
{
    using address_type = size_t;
    using wallet_amount_type = double;

   wallet_amount_type inline AmountFromString(const std::string& str)
   {
      return std::stod(str);
   }

   std::string inline StringFromAmount(wallet_amount_type val)
   {
      return std::to_string(val);
   }

   std::string inline GenerateRandomString(const int len)
   {
       std::string res;
       srand (time(nullptr));
       static const char alphanum[] =
           "0123456789"
           "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
           "abcdefghijklmnopqrstuvwxyz";

       for (int i = 0; i < len; ++i)
       {
           res.push_back(alphanum[rand() % (sizeof(alphanum) - 1)]);
       }

//       res.push_back('\0');
       return res;
   }

   /**
    * \brief Method splits string by 1 character
    * \param text Input text to split
    * \param splitChar Deliminator character
    * \return vector of strings
    */
    inline std::vector<std::string> StringSplit(std::string& text, char splitChar)
    {
        std::vector<std::string> result;
        std::stringstream stream;
        stream.str(text);
        std::string item;
        while(std::getline(stream, item, splitChar))
        {
            result.push_back(item);
        }
        return result;
    }
} // namespace common
} // namespace wallet
